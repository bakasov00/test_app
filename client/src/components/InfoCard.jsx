import React from 'react'
import { Card, CardContent, Typography, Button, Grid } from '@material-ui/core'
import Context from '../context/Context'
import { useRequest } from '../hook/request.hook'

function InfoCard({ client }) {
  const { request } = useRequest()
  const [info, setInfo] = React.useState([])
  const [visible, setVisible] = React.useState(false)
  const { token } = React.useContext(Context)

  React.useEffect(() => {
    setInfo(client)
  }, [client])

  const clickHandler = (id) => {
    request(`/api/client/${id}/?v=${visible}`, { method: 'GET' }, token).then((data) => {
      if (data.errMessage) {
        console.log('Error')
      } else {
        setInfo(data)
        setVisible(!visible)
      }
    })
  }

  return (
    <Card>
      <CardContent>
        <Typography component='p'>
          <b>Имя:</b> {info.name}
        </Typography>
        <Typography component='p'>
          <b>Фамилия:</b> {info.surname}
        </Typography>
        <Typography component='p'>
          <b>Отчество:</b> {info.patronymic}
        </Typography>
        <Typography component='p'>
          <b>Телефон:</b> {info.phone}
        </Typography>
        <Typography component='p'>
          <b>Адрес:</b> {info.adress}
        </Typography>
        <Grid container justify='space-between'>
          <Typography
            component='p'
            style={{
              maxWidth: '600px',
              // minWidth: 280,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            }}>
            <b>ИНН:</b> {info.inn}
          </Typography>
          <Button onClick={() => clickHandler(info._id)} variant='outlined' color='primary'>
            {visible ? 'Скрыть' : 'Показать'}
          </Button>
        </Grid>
      </CardContent>
    </Card>
  )
}

export default InfoCard
